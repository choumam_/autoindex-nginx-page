# Autoindex Nginx Page

Styling autoindex Nginx Page. Create using HTML, Bootstrap, CSS, and Font Awesome.

## Getting started
More info about this project, please visit link 
[Membuat Styling Laman Autoindex Nginx](https://www.chotibulstudio.id/2022/01/membuat-styling-laman-autoindex-nginx.html).

## License
This project is lisenced under MIT license. And other assets like Bootstrap, Font Awesome, and some images are licensed like their respective creators' licenses.
